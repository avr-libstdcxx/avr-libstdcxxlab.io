## Static site for the documentation of the freestanding proposal implementation

This is the source for the static site used to document the freestanding proposal [P0829](wg21.link/p0829) and subsequent proposals.
