---
layout: page
title: About
permalink: /about/
---

This is the documentation site for the AVR implementation of the C++ standard library using the GNU compiler.

All trademarks are the property of their owners.
